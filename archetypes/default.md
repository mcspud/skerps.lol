---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: {{ .Date }}
draft: true
tags: ["replace me", "replace me also"]
showDate: true
showDateUpdated: true
showHeadingAnchors: true
showPagination: true
showReadingTime: true
showTableOfContents:  true
showTaxonomies: true 
showWordCount: true
showSummary: true
showViews: true
---
