---
title: "Analytics"
description: "Site data"
date: 2023-10-28T12:26:26-05:00
draft: false
tags: ["analytics"]
showHero: true
showDate: true
showDateUpdated: true
showHeadingAnchors: true
showPagination: true
showReadingTime: true
showTableOfContents: true
showTaxonomies: true 
showWordCount: true
showSummary: true
showViews: true
layout: "page"
---

<iframe 
  plausible-embed 
  src="https://plausible.io/share/skerps.lol?auth=GXCTCB49L_kQdJ3ISlKdF&embed=true&theme=dark" 
  scrolling="no" 
  frameborder="0" 
  loading="lazy" 
  style="min-height: 2000px; width: 80vw;"
  class="md:h-[3000] lg:h-[1800]"></iframe>
<div style="font-size: 14px; padding-bottom: 14px;">Stats powered by <a target="_blank" style="color: #4F46E5; text-decoration: underline;" href="https://plausible.io">Plausible Analytics</a></div>
<script async src="/api/embed-host"></script>
