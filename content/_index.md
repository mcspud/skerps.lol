---
title: "skerps.lol"
description: "Fight the skeptics at all opportunities."
tags: ["introduction", "preface"]
---

<div class="text-left">
  Welcome.  

  If you're a new reader you probably ended up here via the [UnchartedX Twitch](https://www.twitch.tv/unchartedx) channel.  In that case you should [see what we are about]({{< ref "/start-here">}}).  
  <br />
  In a single phrase it can be *roughly* summarised as

  >Mankind is the remanents of much older civilisation.  Evidence of this litters the globe, but is most prominently displayed in the ancient Egyptian megaliths like the pyramids. Most self-proclaimed skeptics, rather than arguing the facts resort to baseless appeals to authority.  
  >
  >This site exists to combat these people.


  `skerps.lol` is a community driven site.  It lives on a [public gitlab repository](https://gitlab.com/mcspud/skerps.lol) and is hosted by Netlify.  The reason for this is:
  
  - We want people to create pull requests 
  - Edit histories and changes are publicly viewable and can't be surreptiously obsfucated
  - In the case your PR isn't accepted I don't believe that the work goes into the great garbage bin in the bitscape.  You are free to fork the repo and host your own version with your changes as you see fit.

</div>

<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "triangle-exclamation" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">This is a demo of the <code id="layout">background</code> layout.</span>
    <button
      id="switch-layout-button"
      class="px-4 !text-neutral !no-underline rounded-md bg-primary-600 hover:!bg-primary-500 dark:bg-primary-800 dark:hover:!bg-primary-700"
    >
      Switch layout &orarr;
    </button>
  </span>
</div>

Explore the  to get a feel for what Blowfish can do. If you like what you see, check out the project on <a target="_blank" href="https://github.com/nunocoracao/blowfish">Github</a> or read the  to get started.

{{< icon "gitlab" >}}
